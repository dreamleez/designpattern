# designpattern

#### 项目介绍
1. 使用了PHP实现的设计模式
2. 最简洁易懂高可读的基础架构，
3. 通过简单的统一的实例化xxxClient()对象即可使用


#### 使用实例

使用命令模式：
```$xslt

$c = new CommandClient('build mvc admin y'); // 一个命令对应一个方法，如build mvc => buildMvc()
$res = $c->send(); //发送命令
print_r($res);

```


#### 软件架构









