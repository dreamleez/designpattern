<?php

/************************************************************
 * Copyright (C), 1993-~, Dacelve. Tech., Ltd.
 * FileName : InterfaceCommand.php
 * Author   : Lizhijian
 * Version  : 1.0
 * Date     : 2018/6/6 10:26
 * Description   :
 * Main Function :
 * History  :
 * <author>    <time>    <version >    <desc>
 * Lizhijian   2018/6/6   1.0          init
 ***********************************************************/

namespace Command;

/**
 * 抽象接口
 * Interface InterfaceCommand
 * @package Command
 */
interface InterfaceCommand
{
    function execute();
}
