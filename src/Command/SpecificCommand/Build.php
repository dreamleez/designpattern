<?php
/************************************************************
 * Copyright (C), 1993-~, Dacelve. Tech., Ltd.
 * FileName : BuildCommand.php
 * Author   : Lizhijian
 * Version  : 1.0
 * Date     : 2018/6/6 10:29
 * Description   :
 * Main Function :
 * History  :
 * <author>    <time>    <version >    <desc>
 * Lizhijian   2018/6/6   1.0          init
 ***********************************************************/

namespace Command\SpecificCommand;

use Command\InterfaceCommand;
use Command\Receiver;

/**
 * 具体命令 build
 * Class BuildCommand
 * @package Command
 * Fashion:
 */
class Build implements InterfaceCommand
{
    private $receiver;

    function __construct(Receiver $receiver)
    {
        $this->receiver = $receiver;
    }

    /**
     * 调用Receiver里的方法
     * @Description
     * @return mixed
     * @example
     * @author Lizhijian
     * @since 2018/6/8 15:11
     */
    function execute(...$params)
    {
        // 执行对应的action
        $cmd    = $this->receiver->cmd;
        $action = "{$cmd}" . ucwords($this->receiver->action);

        echo '<pre>';
        return $this->receiver->$action($params[0]);
    }
}
