<?php
/************************************************************
 * Copyright (C), 1993-~, Dacelve. Tech., Ltd.
 * FileName : CommandClient.php
 * Author   : Lizhijian
 * Version  : 1.0
 * Date     : 2018/6/6 10:35
 * Description   :
 * 客户端->命令接收者Receiver->具体命令类XXX->请求者Invoker
 *               ↑__________调用执行命令__________↓
 *
 * 客户端：发送命令
 * 接收者：接收命令，并根据命令传递给相应的具体命令者
 * 具体命令者：持有接收者(方便调用)
 * 请求者：决定使用哪个命令
 *
 * 想拓展命令，只要在SpecificCommand目录下新建相应的命令类，然后在Receiver.php里编写命令处理方法即可
 *
 * Main Function :
 * History  :
 * <author>    <time>    <version >    <desc>
 * Lizhijian   2018/6/6   1.0          init
 ***********************************************************/

namespace Command;

/**
 * 客户端
 * Class CommandClient
 * @package Command
 * Fashion:
 */
class CommandClient{

    protected $error;
    protected $command;

    function __construct($command){
        $this->command = $command;
    }

    /**
     * 发送命令
     * @Description
     * @return string
     * @example
     * @author Lizhijian
     * @since 2018/6/8 9:25
     */
    function send(){
        $commands = explode(' ', $this->command);

        $cmd    = $commands[0];
        $action = $commands[1];
        unset($commands[0]);
        unset($commands[1]);
        sort($commands);

        $commandClass    = '\Command\SpecificCommand\\'.ucwords($cmd);// 找到相应的具体命令类
        if(!class_exists($commandClass)){
            $this->error = '没有找到该指令';
            return $this->error;
        }

        $receiver      = new Receiver($cmd, $action);//命令接收者
        $commandObj    = new $commandClass($receiver);//具体命令角色
        $invoker = new Invoker($commandObj);//请求者
        $res = $invoker->executeCommand($commands);
        return $res;
    }
}

