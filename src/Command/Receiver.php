<?php
/************************************************************
 * Copyright (C), 1993-~, Dacelve. Tech., Ltd.
 * FileName : Receiver.php
 * Author   : Lizhijian
 * Version  : 1.0
 * Date     : 2018/6/6 10:30
 * Description   :命令接收者，执行具体命令角色的命令
 * Main Function :
 * History  :
 * <author>    <time>    <version >    <desc>
 * Lizhijian   2018/6/6   1.0          init
 ***********************************************************/

namespace Command;

/**
 * 接收者
 * Class Receiver
 * @package Command
 * Fashion:
 */
class Receiver
{
    public $cmd;
    public $action;
    public $config;

    function __construct($cmd, $action)
    {
        $this->cmd    = $cmd;
        $this->action = $action;
        $this->config = include __DIR__ . '/../Config.php';
    }

    /*** 以下是与具体命令类对应的具体命令的执行方法 ***/

    /**
     * 构造后台MVC方法
     * @Description
     * @example
     * @author Lizhijian
     * @since 2018/6/6 14:50
     */
    function buildMvc($params)
    {
        echo 'buildMvc';
    }
}
