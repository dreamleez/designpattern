<?php
/************************************************************
 * Copyright (C), 1993-~, Dacelve. Tech., Ltd.
 * FileName : Invoker.php
 * Author   : Lizhijian
 * Version  : 1.0
 * Date     : 2018/6/6 10:30
 * Description   : 请求者，请求具体命令的执行
 * Main Function :
 * History  :
 * <author>    <time>    <version >    <desc>
 * Lizhijian   2018/6/6   1.0          init
 ***********************************************************/

namespace Command;

/**
 * 请求者
 * Class Invoker
 * @package Command
 * Fashion:
 */
class Invoker
{
    private $concreteCommand;

    function __construct($concreteCommand)
    {
        $this->concreteCommand = $concreteCommand;
    }

    function executeCommand($params)
    {
        return $this->concreteCommand->execute($params);
    }
}
