<?php
/************************************************************
 * Copyright (C), 1993-~, Dacelve. Tech., Ltd.
 * FileName : config.php
 * Author   : Lizhijian
 * Version  : 1.0
 * Date     : 2018/6/6 14:53
 * Description   : 设计模式的配置文件
 * Main Function :
 * History  :
 * <author>    <time>    <version >    <desc>
 * Lizhijian   2018/6/6   1.0          init
 ***********************************************************/


return array(
    'command_pattern' => array(
        'test' => 'test/'
    )
);

